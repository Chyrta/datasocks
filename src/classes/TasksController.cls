public with sharing class TasksController {
	
	public List<Task__c> tasks;
	public Task__c task {get; set; }
	public ID taskItemId {get; set;}
	private string taskParameter = 'taskItemId';
	
	public List<Task__c> getTasks() {
		if (tasks == null) {
			tasks = [SELECT Id, Name, Completed__c, Deadline__c FROM Task__c ORDER BY Position__c];
		}
		return tasks;
	}
	
	public PageReference deleteTask() {
   		Task__c todel = [SELECT Id FROM Task__c WHERE Id =:taskItemId];
  		delete toDel;
    	PageReference home = Page.TaskList;
    	home.setRedirect(true);
    	return home;
   	}
	
	public PageReference editTaskPage() {
		PageReference editPage = Page.TaskEdit;
		editPage.getParameters().put(taskParameter, taskItemId);
		return editPage;
	}
	
	
}