@isTest
private class TaskControllerTest {
	
	// Test for null list
	static testmethod void TestNullList() {
		TasksController controller = new TasksController();
		PageReference pageRef = Page.TaskList;
        Test.setCurrentPage(pageRef);
		
		List<Task__c> tasks = controller.getTasks();
		System.assertEquals(0, tasks.size());
	}
	
	// Test listing items
	static testmethod void TestShowingAddedItem() {
		Date enteringDate = Date.newInstance(2015, 10, 26);
		Task__c task = new Task__c(Name='Become a Salesforce Java Developer at Datarockets', Completed__c=true, Deadline__c=enteringDate);
		insert task;
		
		TasksController controller = new TasksController();
		PageReference pageRef = Page.TaskList;
		Test.setCurrentPage(pageRef);
		
		List<Task__c> tasks = controller.getTasks();
		System.assertEquals(1, tasks.size());
	}
	
	// Test deleting task from controller
	static testmethod void TestDeletingItem() {
		Date enteringDate = Date.newInstance(2015, 10, 26);
		Task__c task = new Task__c(Name='Become a Salesforce Java Developer at Datarockets', Completed__c=true, Deadline__c=enteringDate);
		insert task;

		TasksController controller = new TasksController();
		List<Task__c> tasks = controller.getTasks();
		System.assertEquals(1, tasks.size());
		
		PageReference pageRef = Page.TaskList;
		Test.setCurrentPage(pageRef);
		
		controller.taskItemId = task.Id;
		controller.deleteTask();
		
		// Reloading page
		controller = new TasksController();
		tasks = controller.getTasks();
		System.assertEquals(0, tasks.size());
	}
	
	// Test link for editing task
	static testmethod void TestEditPageLink() {
		
		Date enteringDate = Date.newInstance(2015, 10, 26);
		Task__c task = new Task__c(Name='Become a Salesforce Java Developer at Datarockets', Completed__c=true, Deadline__c=enteringDate);
		insert task;
		
		TasksController controller = new TasksController();
		PageReference pageRef = controller.editTaskPage();
		
		System.assertNotEquals(null, pageRef);
		System.assertEquals('/apex/taskedit', pageRef.getUrl());
		
		pageRef.getParameters().put('taskItemId', task.Id);
		
		Map<String, String> parameters = pageRef.getParameters();
		System.assertEquals(1, parameters.values().size());
		System.assertEquals(task.Id, parameters.get('taskItemId'));
		
	}
	
}