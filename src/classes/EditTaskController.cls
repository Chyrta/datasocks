public with sharing class EditTaskController {

	public Task__c task {get; set;}
	public ID taskItemId = ApexPages.currentPage().getParameters().get('taskItemId');

	public EditTaskController() {
		if (taskItemId == null) {
			task = new Task__c();	
		} else {
			task = [SELECT Name, Deadline__c, Completed__c from Task__c where Id = :taskItemId];
		}	
	}

	public void submit() {
		try {
			if (taskItemId != null) {
	   			update task;
			} else {
				insert task;
			}
		} catch (DmlException e) {
			System.debug(e);
		}
	}
	
	public PageReference loadList() {
		if (isFieldsReady()) {
			this.submit();
			PageReference homePage = Page.TaskList;
	        homePage.setRedirect(true);
			return homePage;
		} else {
			return null;
		}
	}
	
	private boolean isFieldsReady() {
		return task.Name != NULL && task.Deadline__c != NULL;
	}

}