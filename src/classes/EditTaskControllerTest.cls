@isTest
private class EditTaskControllerTest {
	
	static testmethod void TestEditing() {
		
		Date enteringDate = Date.newInstance(2015, 10, 26);
		Task__c task = new Task__c(Name='Become a Salesforce Java Developer at Datarockets', Completed__c=true, Deadline__c=enteringDate);
		insert task;
		
		EditTaskController controller = new EditTaskController();
		
		controller.taskItemId = task.Id;
		controller.task = task;
		controller.task.Name = 'Work harder';
		controller.task.Completed__c = true;
		controller.loadList();
		
		List<Task__c> tasks = [SELECT Id, Name, Completed__c FROM Task__c];
		
		System.assertEquals('Work harder', tasks[0].Name);
		System.assertEquals(true, tasks[0].Completed__c);
		System.assertEquals(1, tasks.size());
		
	}
	
	static testmethod void TestCreating() {
		EditTaskController controller = new EditTaskController();
		
		Date enteringDate = Date.newInstance(2015, 10, 26);
		
		controller.task.Name = 'You rock the data!';
		controller.task.Deadline__c = enteringDate;
		controller.loadList();
		
		List<Task__c> tasks = [SELECT Id, Name, Completed__c FROM Task__c];
		
		System.assertEquals(1, tasks.size());
		System.assertEquals('You rock the data!', tasks[0].Name);
	}
	
	
}